import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe} from '../../servicios/heroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
})
export class HeroesComponent implements OnInit {
//Creo el arreglo Heroes de tipo any
  
  heroes:Heroe [] = [];


  constructor( private _heroesService:HeroesService,    
               private router:Router    
    ) {
    //  console.log("constructor")
   }
 //Necersario para el arrelgo
   ngOnInit() {
   this.heroes = this._heroesService.getHeroes();
  //  console.log(this.heroes);
  }
  //Aqui creo la funcion verHeroe
  verHeroe(idx:number){
    this.router.navigate(['/heroe',idx]);
  }


}




